Summary: command line tool and library for transferring data with URLs
Name: curl
Version: 7.83.1
Release: 1
License: Copyright (c) 1996 - 2022, Daniel Stenberg, <daniel@haxx.se>, and many contributors, see the THANKS file.
Group: Applications/Internet
Source: https://curl.se/download/%{name}-%{version}.tar.bz2
Provides: webclient
URL: http://curl.haxx.se/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: automake
BuildRequires: groff
BuildRequires: krb5-devel
BuildRequires: libidn-devel
BuildRequires: libssh2-devel
BuildRequires: nss-devel
BuildRequires: openldap-devel
BuildRequires: openssh-clients
BuildRequires: openssh-server
BuildRequires: pkgconfig
BuildRequires: stunnel
BuildRequires: zlib-devel
BuildRequires: valgrind
BuildRequires: openssl-devel
BuildRequires: gnutls
BuildRequires: libnghttp2-devel
BuildRequires: mbedtls-devel
Requires: libcurl = %{version}-%{release}

%description
Curl is a command-line tool for transferring data specified with URL syntax.
Find out how to use curl by reading the curl.1 man page or the MANUAL document.
Find out how to install Curl by reading the INSTALL document.

%package -n libcurl
Summary: A library for getting files from web servers
Group: Development/Libraries
Requires: libssh2%{?_isa} >= %{libssh2_version}

# require a new enough version of nss-pem to avoid regression in yum (#1610998)
Requires: nss-pem%{?_isa} >= 1.0.3-5

%description -n libcurl
libcurl is a free and easy-to-use client-side URL transfer library, supporting
FTP, FTPS, HTTP, HTTPS, SCP, SFTP, TFTP, TELNET, DICT, LDAP, LDAPS, FILE, IMAP,
SMTP, POP3 and RTSP. libcurl supports SSL certificates, HTTP POST, HTTP PUT,
FTP uploading, HTTP form based upload, proxies, cookies, user+password
authentication (Basic, Digest, NTLM, Negotiate, Kerberos4), file transfer
resume, http proxy tunneling and more.

%package -n libcurl-devel
Summary: Files needed for building applications with libcurl
Group: Development/Libraries
Requires: libcurl = %{version}-%{release}
Provides: curl-devel = %{version}-%{release}
Obsoletes: curl-devel < %{version}-%{release}

%description -n libcurl-devel
The libcurl-devel package includes header files and libraries necessary for
developing programs which use the libcurl library. It contains the API
documentation of the library, too.

%prep
%setup -q
automake

%build
%configure --disable-static \
    --enable-hidden-symbols \
    --enable-ipv6 \
    --enable-ldaps \
    --enable-manual \
    --enable-threaded-resolver \
    --with-gssapi \
    --with-libidn \
    --with-libssh2 \
    --with-nghttp2 \
    --with-nss-deprecated \
    --with-openssl \
    --with-gnutls \
    --with-mbedtls
make %{?_smp_mflags}

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT

%post -n libcurl -p /sbin/ldconfig

%postun -n libcurl -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_bindir}/curl
%{_mandir}/man1/curl.1*

%files -n libcurl
%defattr(-,root,root,-)
%{_libdir}/libcurl.so.*

%files -n libcurl-devel
%defattr(-,root,root,-)
%doc docs/examples/*.c docs/examples/Makefile.example
%{_bindir}/curl-config*
%{_includedir}/curl
%{_libdir}/libcurl.*
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man1/curl-config.1*
%{_mandir}/man3/*
%{_datadir}/aclocal/libcurl.m4

%changelog
* Mon Jun 20 2022 DDC <564964001@qq.com> 7.83.1-1
- First shot.