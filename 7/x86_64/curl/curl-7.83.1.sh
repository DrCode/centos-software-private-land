#!/bin/bash

source ./tool.sh

name='curl'
version='7.83.1'
suffix='tar.bz2'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://curl.se/download/"${name}-${version}.${suffix}"
fi

QA_RPATHS=$[ 0x0001|0x0010 ] rpmbuild -ba "${_root}/rpmbuild/SPECS/${name}.spec"