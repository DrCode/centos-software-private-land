Summary: A free and portable font rendering engine
Name: libjpeg-turbo
Version: 2.1.3
Release: 1
License: IJG and BSD and zlib
Group: Applications/Tools
Source: https://github.com/libjpeg-turbo/libjpeg-turbo/%{name}-%{version}.tar.gz
URL: https://libjpeg-turbo.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: gcc
BuildRequires: cmake
BuildRequires: yasm

%description
libjpeg-turbo is a JPEG image codec that uses SIMD instructions to accelerate baseline JPEG compression and decompression on x86, x86-64, Arm, PowerPC,
and MIPS systems, as well as progressive JPEG compression on x86, x86-64, and Arm systems.
On such systems, libjpeg-turbo is generally 2-6x as fast as libjpeg, all else being equal. On other types of systems,
libjpeg-turbo can still outperform libjpeg by a significant amount, by virtue of its highly-optimized Huffman coding routines.
In many cases, the performance of libjpeg-turbo rivals that of proprietary high-speed JPEG codecs.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%__mkdir _install build && cd build
cmake .. -D BUILD_SHARED_LIBS=ON -D CMAKE_INSTALL_PREFIX=%{_builddir}/%{name}-%{version}/_install/usr
%__make && %__make install

%install
cd _install
%__cp -rf * %{buildroot}

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_bindir}/*
%{_mandir}/man1/*
%{_includedir}/*
%{_datadir}/doc/*


%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 2.1.3-1
- First shot.