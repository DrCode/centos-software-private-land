Summary: A library of functions for manipulating PNG image format files
Name: libpng
Version: 1.6.37
Release: 1
License: zlib
Group: Applications/Tools
Source: https://github.com/glennrp/libpng/archive/refs/tags/%{name}-%{version}.tar.gz
URL: http://www.libpng.org/pub/png/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}

%description
The libpng package contains a library of functions for creating and manipulating PNG (Portable Network Graphics) image format files.
PNG is a bit-mapped graphics format similar to the GIF format.
PNG was created to replace the GIF format, since GIF uses a patented datacompression algorithm.
Libpng should be installed if you need to manipulate PNG format imagefiles.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name}-library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%configure
%make_build

%install
%make_install

#安装后
%post -n %{name}-devel
/sbin/ldconfig -l
#卸载后
%postun -n %{name}-devel
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_bindir}/*
%{_includedir}/*
%{_mandir}/man3/*
%{_mandir}/man5/*


%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 1.6.37.0-1
- First shot.