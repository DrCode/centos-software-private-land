#!/bin/bash

source ./tool.sh

name='apr-util'
version='1.6.1'
suffix='tar.bz2'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://dlcdn.apache.org//apr/"${name}-${version}.${suffix}"
fi

# 无报错模式-通过源码文件包编译
rpmbuild -ta "${_root}/rpmbuild/SOURCES/${name}-${version}.${suffix}"
