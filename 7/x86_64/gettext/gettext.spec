Summary: This library provides an implementation, for use on systems which don't have one, or whose implementation cannot convert from/to Unicode.
Name: gettext
Version: 0.21
Release: 1
License: GPL
Group: Applications/Tools
Source: https://ftp.gnu.org/gnu/gettext/%{name}-%{version}.tar.gz
URL: https://www.gnu.org/software/gettext/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
Requires: %{name}-library = %{version}-%{release}
Provides: %{name}-library = %{version}-%{release}
Obsoletes: %{name}-library < %{version}-%{release}

%description
Usually, programs are written and documented in English,
and use English at execution time for interacting with users.
This is true not only from within GNU, but also in a great deal of proprietary and free software.
Using a common language is quite handy for communication between developers, maintainers and users from all countries.
On the other hand, most people are less comfortable with English than with their own native language,
and would rather be using their mother tongue for day to day's work, as far as possible.
Many would simply love seeing their computer screen showing a lot less of English, and far more of their own language.

%package -n gettext-library
Summary: A library for gettext
Group: Applications/Libraries

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}
Requires: %{name}-library = %{version}-%{release}
Provides: %{name}-library = %{version}-%{release}
Obsoletes: %{name}-library < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%__mkdir _install
%configure
%make_build
%{__make} install DESTDIR=%{_builddir}/%{name}-%{version}/_install

%install
cd _install
%__cp -rf * %{buildroot}

#安装后
%post -n %{name}-library
/sbin/ldconfig -l
#卸载后
%postun -n %{name}-library
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_mandir}/man1/*

%files -n %{name}-library
%defattr(-,root,root,-)
%{_datadir}/locale/*
%{_libdir}/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_datadir}/aclocal/*
%{_datadir}/doc/*
%{_datadir}/gettext/*
%{_datadir}/gettext-0.21/*
%{_datadir}/info/*
%{_mandir}/man3/*


%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 0.21-1
- First shot.