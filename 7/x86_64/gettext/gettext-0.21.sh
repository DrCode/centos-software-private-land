#!/bin/bash

source ./tool.sh

name='gettext'
version='0.21'
suffix='tar.gz'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://ftp.gnu.org/gnu/gettext/"${name}-${version}.${suffix}"
fi

QA_RPATHS=$[ 0x0001|0x0002 ] rpmbuild -ba "${_root}/rpmbuild/SPECS/${name}.spec"
