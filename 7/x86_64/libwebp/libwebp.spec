Summary: Library for manipulating GIF format image files
Name: libwebp
Version: 1.2.2
Release: 1
License: BSD
Group: Applications/Tools
Source: https://storage.googleapis.com/downloads.webmproject.org/releases/webp/%{name}-%{version}.tar.gz
URL: https://developers.google.com/speed/webp
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: gcc
BuildRequires: make
BuildRequires: libjpeg-turbo-devel
BuildRequires: libpng-devel
BuildRequires: libtiff-devel
BuildRequires: giflib-devel

%description
WebP is a modern image format that provides superior lossless and lossy compression for images on the web.
Using WebP, webmasters and web developers can create smaller, richer images that make the web faster.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%configure
%make_build

%install
%make_install

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_bindir}/*
%{_includedir}/*
%{_mandir}/man1/*

%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 1.2.2-1
- First shot.