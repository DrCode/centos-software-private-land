#!/bin/bash

source ./tool.sh

name='libiconv'
version='1.17'
suffix='tar.gz'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://ftp.gnu.org/pub/gnu/libiconv/"${name}-${version}.${suffix}"
fi

# 无报错模式-通过源码文件包编译
QA_RPATHS=$[ 0x0001|0x0002 ] rpmbuild -ba "${_root}/rpmbuild/SPECS/${name}.spec"
