Summary: This library provides an implementation, for use on systems which don't have one, or whose implementation cannot convert from/to Unicode.
Name: libiconv
Version: 1.17
Release: 1
License: GPL
Group: Applications/Tools
Source: https://ftp.gnu.org/pub/gnu/libiconv/%{name}-%{version}.tar.gz
URL: https://www.gnu.org/software/libiconv/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
Requires: libiconv-library = %{version}-%{release}

%description
International text is mostly encoded in Unicode. For historical reasons, however,
it is sometimes still encoded using a language or country dependent character encoding.
With the advent of the internet and the frequent exchange of text across countries - even the viewing of a web page from a foreign country is a "text exchange" in this context -,
conversions between these encodings have become a necessity.
In particular, computers with the Windows operating system still operate in locale with a traditional (limited) character encoding.
Some programs, like mailers and web browsers, must be able to convert between a given text encoding and the user's encoding.
Other programs internally store strings in Unicode, to facilitate internal processing, and need to convert between internal string representation (Unicode) and external string representation (a traditional encoding) when they are doing I/O.
GNU libiconv is a conversion library for both kinds of applications.

%package -n libiconv-library
Summary: A library for libiconv
Group: Applications/Libraries

%description -n libiconv-library
A library for libiconv

%package -n libiconv-devel
Summary: A library for libiconv
Group: Applications/Libraries
Requires: libiconv = %{version}-%{release}
Requires: libiconv-library = %{version}-%{release}

%description -n libiconv-devel
The libiconv-devel package includes header files and libraries necessary for
developing programs which use the libiconv library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%__mkdir _install
%configure
%make_build
%{__make} install DESTDIR=%{_builddir}/%{name}-%{version}/_install

%install
cd _install
%__cp -rf * %{buildroot}

#安装后
%post -n libiconv-library
/sbin/ldconfig -l
#卸载后
%postun -n libiconv-library
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/iconv
%{_mandir}/man1/*

%files -n libiconv-library
%defattr(-,root,root,-)
%{_datadir}/locale/*
%{_libdir}/*

%files -n libiconv-devel
%defattr(-,root,root,-)
%{_datadir}/doc/*
%{_mandir}/man3/*
%{_includedir}/*

%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 1.17.0-1
- First shot.