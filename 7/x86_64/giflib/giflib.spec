Summary: Library for manipulating GIF format image files
Name: giflib
Version: 5.2.1
Release: 1
License: MIT
Group: Applications/Tools
Source: https://udomain.dl.sourceforge.net/project/giflib/%{name}-%{version}.tar.gz
URL: http://www.sourceforge.net/projects/giflib/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: gcc

%description
The giflib package contains a shared library of functions for loading and saving GIF format image files.
It is API and ABI compatible with libungif, the library which supported uncompressed GIFs while the Unisys LZW patent was in effect.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%make_build

%install
%make_install

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_exec_prefix}/local/lib/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_exec_prefix}/local/bin/*
%{_exec_prefix}/local/share/man/man1/*
%{_exec_prefix}/local/include/*


%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 5.2.1-1
- First shot.