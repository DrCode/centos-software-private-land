#!/bin/bash

source ./tool.sh

name='giflib'
version='5.2.1'
suffix='tar.gz'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://udomain.dl.sourceforge.net/project/giflib/${name}-${version}.${suffix}
fi

rpmbuild -ba "${_root}/rpmbuild/SPECS/${name}.spec"
