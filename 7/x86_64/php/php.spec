Summary: PHP scripting language for creating dynamic web sites.
Name: php
Version: 7.4.30
Release: 1
License: PHP and Zend and BSD
Group: Applications/Internet
Source: https://www.php.net/distributions/%{name}-%{version}.tar.gz
URL: https://www.php.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires:libcurl-devel
BuildRequires:libxml2-devel
BuildRequires:xmlrpc-c-devel
BuildRequires:openssl-devel
BuildRequires:xz-devel
BuildRequires:libzstd-devel
BuildRequires:libmcrypt-devel
BuildRequires:libsodium-devel
BuildRequires:gmp-devel
BuildRequires:sqlite-devel
BuildRequires:oniguruma-devel
BuildRequires:freetype-devel
BuildRequires:gettext-devel
BuildRequires:libiconv-devel
BuildRequires:giflib-devel
BuildRequires:libjpeg-turbo-devel
BuildRequires:libpng-devel
BuildRequires:libwebp-devel
BuildRequires:gd-devel
BuildRequires:libzip-devel
BuildRequires:bzip2-devel

Requires: perl
Requires: curl
Requires: libxml2
Requires: xmlrpc-c
Requires: openssl
Requires: xz
Requires: libzstd
Requires: libmcrypt
Requires: libsodium
Requires: gmp
Requires: sqlite
Requires: oniguruma
Requires: freetype
Requires: gettext
Requires: libiconv
Requires: giflib
Requires: libjpeg-turbo
Requires: libpng
Requires: libwebp
Requires: libzip
Requires: gd
Requires: zlib
Requires: bzip2

%description
PHP is an HTML-embedded scripting language. PHP attempts to make it easy for developers to write dynamically generated web pages.
PHP also offers built-in database integration for several commercial and non-commercial database management systems,
so writing a database-enabled webpage with PHP is fairly simple.
The most common use of PHP coding is probably as a replacement for CGI scripts.

%prep
%setup -q

%build
mkdir -p /etc/%{name}-%{version}
%_configure --prefix=/usr/local/%{name}-%{version} --enable-fpm --with-fpm-user=www --with-fpm-group=www --enable-mbstring --with-curl --enable-sockets --enable-calendar --enable-bcmath --enable-inline-optimization --enable-mbregex --enable-opcache --enable-pcntl --enable-shmop --enable-sysvsem --with-mhash --with-mcrypt --with-sodium --with-openssl --with-zip --with-zlib --with-bz2 --with-jpeg --with-png --with-webp --with-freetype --enable-gd --enable-exif --with-gettext --with-gmp --with-libxml --with-xmlrpc --with-pear --with-sodium --with-mysql=mysqlnd --with-pdo-mysql=mysqlnd --with-sqlite3

%make_build

%install
%make_install

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
#%{_bindir}/zip*
#%{_mandir}/man1/*
#%{_libdir}/libzip.so.5*

#%files -n %{name}-devel
#%defattr(-,root,root,-)
#%{_includedir}/*
#%{_libdir}/libzip.so
#%{_libdir}/cmake/*
#%{_libdir}/pkgconfig/*
#%{_mandir}/man3/*

%changelog
* Mon Jun 20 2022 DDC <564964001@qq.com> 7.4.30-1
- First shot.