#!/bin/bash

# 判断文件是否存在
isFile(){
  if [ ! -e $1 ]; then
    return 1
  fi
  return 0
}
# 判断目录
isDir(){
  if [ ! -d $1 ]; then
    return 1
  fi
  return 0
}