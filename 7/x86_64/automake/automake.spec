Name:       automake
Version:    1.16.5
Release:    1
Summary:    Automatically generate Makefiles
Group:      Development/Tools
License:    GNU GPL
URL:        http://www.gnu.org/software/automake/
Source0:    https://ftp.gnu.org/gnu/automake/automake-%{version}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires:  autoconf
Requires:   autoconf
Requires(post):  /sbin/install-info
Requires(preun): /sbin/install-info
AutoReq: no

%description
Automake is a tool for automatically generating Makefiles compliant with the
GNU Coding Standards.

%prep
%setup -q

%build
%configure --docdir=/usr/share/doc/automake-1.16
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/automake.info.gz %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
/sbin/install-info --delete %{_infodir}/automake.info.gz %{_infodir}/dir || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS README THANKS NEWS
%{_bindir}/*
%{_infodir}/*.info*
%{_datadir}/automake-1.16
%{_datadir}/aclocal-1.16
%{_datadir}/aclocal/README
%{_mandir}/man1/*
%{_docdir}/*/*

%changelog
* Mon Jun 20 2022 DDC <564964001@qq.com> 1.16.5-1
- First shot.