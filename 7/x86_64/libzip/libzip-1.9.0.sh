#!/bin/bash

source ./tool.sh

name='libzip'
version='1.9.0'
suffix='tar.gz'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://libzip.org/download/"${name}-${version}.${suffix}"
fi

# 无报错模式-通过源码文件包编译
rpmbuild -ba "${_root}/rpmbuild/SPECS/${name}.spec"
