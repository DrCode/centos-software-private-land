Summary: This is libzip, a C library for reading, creating, and modifying zip and zip64 archives.
Name: libzip
Version: 1.9.0
Release: 1
License: BSD
Group: Development/Tools
Source: https://libzip.org/download/%{name}-%{version}.tar.gz
URL: https://libzip.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: cmake
BuildRequires: perl
BuildRequires: zlib
BuildRequires: bzip2-devel
BuildRequires: xz-devel
BuildRequires: libzstd-devel
BuildRequires: openssl-devel

%description
This is libzip, a C library for reading, creating, and modifying zip and zip64 archives.
Files can be added from data buffers, files, or compressed data copied directly from other zip archives.
Changes made without closing the archive can be reverted. Decryption and encryption of Winzip AES and legacy PKware encrypted files is supported.

%package -n %{name}-devel
Summary: Files needed for building applications with %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%__mkdir _install build && cd build
#cmake 编译安装 先安装到自定义位置，在 install 阶段用 cp 命令复制，否则会出现 check-buildroot 中止错误
cmake .. -D BUILD_SHARED_LIBS=ON -D CMAKE_INSTALL_PREFIX=%{_builddir}/%{name}-%{version}/_install/usr
%__make && %__make install

%install
cd _install
%__cp -rf * %{buildroot}

#安装后
%post -n %{name}-devel
/sbin/ldconfig -l
#卸载后
%postun -n %{name}-devel
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/zip*
%{_mandir}/man1/*
%{_libdir}/libzip.so.5*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libzip.so
%{_libdir}/cmake/*
%{_libdir}/pkgconfig/*
%{_mandir}/man3/*

%changelog
* Mon Jun 20 2022 DDC <564964001@qq.com> 3.23.2-1
- First shot.