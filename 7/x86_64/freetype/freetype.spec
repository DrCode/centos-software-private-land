Summary: A free and portable font rendering engine
Name: freetype
Version: 2.12.1
Release: 1
License: (FTL or GPLv2+) and BSD and MIT and Public Domain and zlib with acknowledgement
Group: Applications/Tools
Source: https://download.savannah.gnu.org/releases/freetype/%{name}-%{version}.tar.gz
URL: https://freetype.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: zlib
BuildRequires: bzip2
BuildRequires: libpng

%description
The FreeType engine is a free and portable font rendering engine, developed to provide advanced font support for a variety of platforms and environments.
FreeType is a library which can open and manages font files as well as efficiently load, hint and render individual glyphs.
FreeType is not a font server or a complete text-rendering library.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name} library. It contains the API
documentation of the library, too.

%prep
%setup -q

%build
%configure
%make_build

%install
%make_install

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/*
%{_datadir}/aclocal/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_includedir}/*

%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 2.12.1-1
- First shot.