#!/bin/bash

source ./tool.sh

name='httpd'
version='2.4.54'
suffix='tar.bz2'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -O https://dlcdn.apache.org/"${name}/${name}-${version}.${suffix}"
fi

rpmbuild -ta "${_root}/rpmbuild/SOURCES/${name}-${version}.${suffix}"
