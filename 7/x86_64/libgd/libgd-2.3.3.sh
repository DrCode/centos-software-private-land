#!/bin/bash

source ./tool.sh

name='gd'
version='2.3.3'
suffix='tar.gz'

_root=$(pwd)

#下载

cd "${_root}/rpmbuild/SOURCES"

if isFile "libgd-${name}-${version}.${suffix}"
then
    echo "文件已存在"
else
    echo "文件不存在,需要下载"
    curl -o libgd-{name}-${version}.${suffix} https://github.com/libgd/libgd/archive/refs/tags/gd-"${version}.${suffix}"
fi

rpmbuild -ba "${_root}/rpmbuild/SPECS/lib${name}.spec"
