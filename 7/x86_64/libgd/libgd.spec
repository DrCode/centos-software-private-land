Summary: A library of functions for manipulating PNG image format files
Name: gd
Version: 2.3.3
Release: 1
License: MIT
Group: Applications/Tools
Source: https://github.com/glennrp/libpng/archive/refs/tags/libgd-%{name}-%{version}.tar.gz
URL: https://libgd.github.io/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: freetype-devel
BuildRequires: libjpeg-turbo-devel
BuildRequires: libpng-devel
BuildRequires: libwebp-devel
BuildRequires: zlib-devel
BuildRequires: libtiff-devel
BuildRequires: freetype-devel

%description
The gd graphics library allows your code to quickly draw images complete with lines, arcs, text, multiple colors,
cut and paste from other images, and flood fills, and to write out the result as a PNG or JPEG file.
This is particularly useful in Web applications, where PNG and JPEG are two of the formats accepted for inline images by most browsers.
Note that gd is not a paint program.

%package -n %{name}-devel
Summary: A library for %{name}
Group: Applications/Libraries
Requires: %{name} = %{version}-%{release}
Provides: %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n %{name}-devel
The %{name}-devel package includes header files and libraries necessary for
developing programs which use the %{name}-library. It contains the API
documentation of the library, too.

%prep
%autosetup -c %{name}-%{version}

%build
cd libgd-%{name}-%{version}
sh ./bootstrap.sh
%configure
%make_build
#cd tests
#make check-TESTS

%install
cd libgd-%{name}-%{version}
%make_install

#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_libdir}/*

%files -n %{name}-devel
%defattr(-,root,root,-)
%{_bindir}/*
%{_includedir}/*


%changelog
* Mon Jun 23 2022 DDC <564964001@qq.com> 2.3.3-1
- First shot.