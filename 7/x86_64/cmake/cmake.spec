Summary: command line tool and library for transferring data with URLs
Name: cmake
Version: 3.23.2
Release: 1
License: CMake - Cross Platform Makefile Generator Copyright 2000-2022 Kitware, Inc. and Contributors All rights reserved.
Group: Development/Tools
Source: https://github.com/Kitware/CMake/releases/download/v3.23.2/%{name}-%{version}.tar.gz
URL: https://cmake.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}
BuildRequires: libarchive
BuildRequires: openssl-devel

%description
CMake is an open-source, cross-platform family of tools designed to build, test and package software.
CMake is used to control the software compilation process using simple platform and compiler independent configuration files,
and generate native makefiles and workspaces that can be used in the compiler environment of your choice.
The suite of CMake tools were created by Kitware in response to the need for a powerful,
cross-platform build environment for open-source projects such as ITK and VTK.

%prep
%setup -q

%build
./bootstrap
make %{?_smp_mflags}

%install
%make_install
#安装后
%post
/sbin/ldconfig -l
#卸载后
%postun
/sbin/ldconfig -l

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_exec_prefix}/local/*


%changelog
* Mon Jun 20 2022 DDC <564964001@qq.com> 3.23.2-1
- First shot.