# CentosSoftwarePrivateLand

#### 介绍
本仓库提供一些在 centos 系统上，缺少更新支持的软件二进制包，对应软件的`spec`文件，和编译shell文件


#### 软件架构
所有二进制`rpm`包，来源都是官方源代码编译生成，减少大家的编译时间，减少编译问题。
大家可根据对应系统架构自行下载安装。
同时也可以自行通过shell脚本和`spec`文件自行编译。

> 自行编译一定要记得找一个同系统同架构同版号的纯净环境.


#### 安装教程

1.  下载你想要的软件包，上传至服务器
2.  使用`yum localinstall xxx.rpm` 安装
3.  或者使用`rpm -ivh xxxx.rpm` 安装

#### 使用说明

1.  yum 的好处就算用 localinstall 也能解决依赖问题.

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request